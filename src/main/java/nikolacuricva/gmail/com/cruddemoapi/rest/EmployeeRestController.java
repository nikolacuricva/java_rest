package nikolacuricva.gmail.com.cruddemoapi.rest;

import jakarta.validation.Valid;
import nikolacuricva.gmail.com.cruddemoapi.custom_responses.ErrorResponseTemplate;
import nikolacuricva.gmail.com.cruddemoapi.custom_responses.InsertResponseTemplate;
import nikolacuricva.gmail.com.cruddemoapi.custom_responses.UpdateResponseTemplate;
import nikolacuricva.gmail.com.cruddemoapi.entity.Employee;
import nikolacuricva.gmail.com.cruddemoapi.enumerations.EmployeeEnum;
import nikolacuricva.gmail.com.cruddemoapi.error.NotFoundException;
import nikolacuricva.gmail.com.cruddemoapi.service.EmployeeServiceInterface;
import nikolacuricva.gmail.com.cruddemoapi.service.JPAEmployeeServiceInterface;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.nio.file.Files;
import java.util.List;

@RestController
@RequestMapping("/api")
@Validated
public class EmployeeRestController {

    private final EmployeeServiceInterface employeeService;

    private final JPAEmployeeServiceInterface jpaEmployeeService;

    @Autowired
    public EmployeeRestController(EmployeeServiceInterface employeeService, JPAEmployeeServiceInterface jpaEmployeeService) {
        this.employeeService = employeeService;
        this.jpaEmployeeService = jpaEmployeeService;
    }

    @GetMapping("/employee")
    List<Employee> findAll (
            @RequestParam(
                    required = false,
                    defaultValue = "1"
            ) Integer page,
            @RequestParam(
                    required = false,
                    defaultValue = "10"
            ) Integer limit
    ) {

        return employeeService.findAll(page, limit);
    }

    @GetMapping("employee/{id}")
    Employee findById(@PathVariable Integer id) {
        return employeeService.findById(id);
    }

    @PostMapping("/employee")
    InsertResponseTemplate insert(@Valid() @RequestBody Employee employee) {
        this.employeeService.prepareEmployeeForStore(employee);
        this.employeeService.insert(employee);
//        this.jpaEmployeeService.insert(employee);
        return new InsertResponseTemplate(employee.getId());
    }

    @PutMapping("/employee")
    UpdateResponseTemplate update(@RequestBody Employee employee) throws IOException {
        this.employeeService.prepareEmployeeForStore(employee);
        this.employeeService.update(employee);

        return new UpdateResponseTemplate(employee.getId());
    }

    @GetMapping(
            value = "/employee/preview-file/{fileName}",
            produces = MediaType.APPLICATION_OCTET_STREAM_VALUE
    )
    public ResponseEntity<InputStreamResource> previewFile(@PathVariable String fileName) throws IOException {

        /**
         * IF FILE LOCATED IN "main/resources" DIRECTORY
         * InputStream inputStream = getClass().getResourceAsStream("/data/employee/mongo.png");
         */

        /** WHEREVER FILE IS LOCATED ON FILESYSTEM */
        FileSystemResource resource = new FileSystemResource(EmployeeEnum.FILE_DIR.getFilePath(fileName));

        String fileType = Files.probeContentType(resource.getFile().toPath());

        InputStream inputStream = resource.getInputStream();

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(fileType))
                .body(new InputStreamResource(inputStream));
    }
    @GetMapping(
            value = "/employee/download-file/{fileName}",
            produces = MediaType.APPLICATION_OCTET_STREAM_VALUE
    )
    public @ResponseBody byte[] getFile(@PathVariable String fileName) throws IOException {
        FileSystemResource resource = new FileSystemResource(EmployeeEnum.FILE_DIR.getFilePath(fileName));
        try (InputStream inputStream = resource.getInputStream()) {
            return IOUtils.toByteArray(inputStream);
        }
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponseTemplate> employeeNotFound (NotFoundException ex) {
        ErrorResponseTemplate err = new ErrorResponseTemplate();
        err.setStatusCode(HttpStatus.NOT_FOUND.value());
        err.setErrorMessage(ex.getMessage());
        err.setTime(System.currentTimeMillis());

        return new ResponseEntity<>(err, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponseTemplate> handleRunTimeError (RuntimeException ex) {
        ErrorResponseTemplate err = new ErrorResponseTemplate();
        err.setStatusCode(HttpStatus.NOT_FOUND.value());
        err.setErrorMessage(ex.getMessage());
        err.setTime(System.currentTimeMillis());

        return new ResponseEntity<>(err, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponseTemplate> handleValidationError (MethodArgumentNotValidException ex) {
        ErrorResponseTemplate err = new ErrorResponseTemplate();
        err.setStatusCode(HttpStatus.BAD_REQUEST.value());
        err.setErrorMessage(ex.getFieldError().getDefaultMessage());
        err.setTime(System.currentTimeMillis());

        return new ResponseEntity<>(err, HttpStatus.NOT_FOUND);
    }
}
