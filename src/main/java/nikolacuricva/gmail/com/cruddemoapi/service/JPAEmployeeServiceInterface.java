package nikolacuricva.gmail.com.cruddemoapi.service;

import nikolacuricva.gmail.com.cruddemoapi.entity.Employee;

import java.util.List;

public interface JPAEmployeeServiceInterface {
    // insert method only for example
    Employee insert(Employee employee);
}
