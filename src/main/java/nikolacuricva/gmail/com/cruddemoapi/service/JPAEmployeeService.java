package nikolacuricva.gmail.com.cruddemoapi.service;

import nikolacuricva.gmail.com.cruddemoapi.dao.JPAEmployeeRepositoryInterface;
import nikolacuricva.gmail.com.cruddemoapi.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class JPAEmployeeService implements JPAEmployeeServiceInterface {

    private final JPAEmployeeRepositoryInterface jpaRepository;

    @Autowired
    public JPAEmployeeService(JPAEmployeeRepositoryInterface employeeRepository) {
        this.jpaRepository = employeeRepository;
    }


    @Override
    @Transactional
    public Employee insert(Employee employee) {
        return jpaRepository.save(employee);
    }
}
