package nikolacuricva.gmail.com.cruddemoapi.service;

import nikolacuricva.gmail.com.cruddemoapi.dao.EmployeeRepositoryInterface;
import nikolacuricva.gmail.com.cruddemoapi.entity.Employee;
import nikolacuricva.gmail.com.cruddemoapi.entity.EmployeeDocument;
import nikolacuricva.gmail.com.cruddemoapi.enumerations.EmployeeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

@Service
public class EmployeeService implements EmployeeServiceInterface {

    private EmployeeRepositoryInterface employeeRepository;

    @Autowired
    public EmployeeService(EmployeeRepositoryInterface employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public List<Employee> findAll(Integer page, Integer limit) {
        return this.employeeRepository.findAll(page, limit);
    }

    @Override
    public Employee findById(Integer id) {
        return employeeRepository.findById(id);
    }

    @Override
    @Transactional
    public Employee insert(Employee employee) {
        return employeeRepository.insert(employee);
    }

    @Override
    @Transactional
    public Employee update(Employee employee) {
        return employeeRepository.update(employee);
    }

    public void prepareEmployeeForStore(Employee employee) {
        EmployeeDocument[] employeeDocuments = employee.getDocuments();

        this.storeFiles(employeeDocuments);

        Arrays.stream(employeeDocuments).map(document -> {
            document.setFileContent(null);
            document.setFilePath(EmployeeEnum.FILE_DIR.fileDirPath+"/"+document.getFileName());
            return employeeDocuments;
        }).toArray();

        employee.setDocuments(employeeDocuments);
//        int i = 0;
//        for (EmployeeDocument document : employeeDocuments) {
//            document.setFileContent("");
//            employeeDocuments[i] = document;
//        }
    }

    private void storeFiles(EmployeeDocument[] documents) {
        for (EmployeeDocument document : documents) {
            this.storeFile(document);
        }
    }

    private void storeFile(EmployeeDocument document) {
        byte[] decodedFileBytes = Base64
                .getDecoder()
                .decode(
                    document.getFileContent()
                );

        try (
            OutputStream stream = new FileOutputStream(
            EmployeeEnum.FILE_DIR.fileDirPath
                    + "/"
                    + document.getFileName()
            )
        ) {
            stream.write(decodedFileBytes);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
