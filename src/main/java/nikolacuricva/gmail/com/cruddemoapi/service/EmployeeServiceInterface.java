package nikolacuricva.gmail.com.cruddemoapi.service;

import nikolacuricva.gmail.com.cruddemoapi.entity.Employee;

import java.util.List;

public interface EmployeeServiceInterface {
    List<Employee> findAll(Integer page, Integer limit);
    Employee findById(Integer id);
    Employee insert(Employee employee);
    Employee update(Employee employee);
    void prepareEmployeeForStore(Employee employee);
}
