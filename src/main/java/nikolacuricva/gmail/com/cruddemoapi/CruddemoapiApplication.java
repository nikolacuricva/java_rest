package nikolacuricva.gmail.com.cruddemoapi;

import nikolacuricva.gmail.com.cruddemoapi.document.EmployeeDoc;
import nikolacuricva.gmail.com.cruddemoapi.document_repository.DocumentRepositoryInterface;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
//@ComponentScan({"com.delivery.request"})
//@EntityScan("com.delivery.domain")
//@EnableJpaRepositories("com.delivery.repository")
//@EnableMongoRepositories("nikolacuricva.gmail.com.cruddemoapi.document_repository")
@EnableMongoRepositories
public class CruddemoapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CruddemoapiApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(DocumentRepositoryInterface documentRepository) {
		return args -> {
			EmployeeDoc employee = new EmployeeDoc("Nikola", "Curic", "nikolacuricva@gmail.com");
			documentRepository.insert(employee);
		};
	}

}
