package nikolacuricva.gmail.com.cruddemoapi.enumerations;

public enum EmployeeEnum {
    FILE_DIR("src/data/employee");

    public final String fileDirPath;
    EmployeeEnum(String fileDir) {
        this.fileDirPath = fileDir;
    }

    public final String getFilePath(String fileName) {
        return FILE_DIR.fileDirPath + "/" + fileName;
    }
}
