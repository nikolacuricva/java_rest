package nikolacuricva.gmail.com.cruddemoapi.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;


@Entity
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @NotEmpty
    @Size(min = 2, message = "First name have to be longer than 1 character")
    @Column(name = "first_name")
    private String firstName;

    @NotEmpty
    @Size(min = 2, message = "Nevalja")
    @Column(name = "last_name")
    private String lastName;

    @NotEmpty
    @Email
    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "documents", columnDefinition = "JSON")
    @JdbcTypeCode(SqlTypes.JSON)
    private EmployeeDocument[] documents;

    public Employee() {
    }

    public Employee(String firstName, String lastName, String email, EmployeeDocument[] documents) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.documents = documents;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public EmployeeDocument[] getDocuments() {
        return documents;
    }

    public void setDocuments(EmployeeDocument[] documents) {
        this.documents = documents;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
