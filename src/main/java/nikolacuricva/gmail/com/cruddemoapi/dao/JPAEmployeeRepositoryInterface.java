package nikolacuricva.gmail.com.cruddemoapi.dao;

import nikolacuricva.gmail.com.cruddemoapi.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JPAEmployeeRepositoryInterface extends JpaRepository<Employee, Integer> {

}
