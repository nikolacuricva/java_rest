package nikolacuricva.gmail.com.cruddemoapi.dao;

import nikolacuricva.gmail.com.cruddemoapi.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepositoryInterface {

    List<Employee> findAll(Integer page, Integer limit);

    Employee findById(Integer id);

    Employee insert(Employee employee);

    Employee update(Employee employee);

    void delete (Employee employee);
}
