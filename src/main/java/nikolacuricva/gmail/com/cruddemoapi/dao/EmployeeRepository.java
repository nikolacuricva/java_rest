package nikolacuricva.gmail.com.cruddemoapi.dao;

import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import nikolacuricva.gmail.com.cruddemoapi.entity.Employee;
import nikolacuricva.gmail.com.cruddemoapi.error.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EmployeeRepository implements EmployeeRepositoryInterface {
    private EntityManager entityManager;

    @Autowired
    public EmployeeRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<Employee> findAll(Integer page, Integer limit) {
        TypedQuery<Employee> employeeTypedQuery = this.entityManager
                .createQuery("FROM Employee", Employee.class)
                .setFirstResult((page-1)*limit)
                .setMaxResults(limit);
        return employeeTypedQuery.getResultList();
    }

    @Override
    public Employee findById(Integer id) {
        if (id < 1) {
            throw new NotFoundException("Passed id have to be greater then 0.");
        }
        return this.entityManager.find(Employee.class, id);
    }

    @Override
    public Employee insert(Employee employee) {
        this.entityManager.merge(employee);
        return employee;
    }

    @Override
    public Employee update(Employee employee) {
        Employee employeeForUpdate = this.findById(employee.getId());
        if (employeeForUpdate != null) {
            this.entityManager.merge(employee);
            return employee;
        }
        employee.setId(0);
        return employee;
    }

    @Override
    public void delete(Employee employee) {
        this.entityManager.remove(employee);
    }
}
