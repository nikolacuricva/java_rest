package nikolacuricva.gmail.com.cruddemoapi.custom_responses;

public class InsertResponseTemplate {
    private Integer insertedId;

    public InsertResponseTemplate(Integer insertedId) {
        this.insertedId = insertedId;
    }

    public Integer getInsertedId() {
        return insertedId;
    }

    public void setInsertedId(Integer insertedId) {
        this.insertedId = insertedId;
    }
}
