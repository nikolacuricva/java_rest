package nikolacuricva.gmail.com.cruddemoapi.custom_responses;

public class UpdateResponseTemplate {
    private Integer updatedId;

    public UpdateResponseTemplate(Integer updatedId) {
        this.updatedId = updatedId;
    }

    public Integer getUpdatedId() {
        return updatedId;
    }

    public void setUpdatedId(Integer updatedId) {
        this.updatedId = updatedId;
    }
}
