package nikolacuricva.gmail.com.cruddemoapi.custom_responses;

public class ErrorResponseTemplate {

    private Integer statusCode;

    private String errorMessage;

    private Long time;

    public ErrorResponseTemplate() {
    }

    public ErrorResponseTemplate(Integer statusCode, String errorMessage, Long time) {
        this.statusCode = statusCode;
        this.errorMessage = errorMessage;
        this.time = time;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
