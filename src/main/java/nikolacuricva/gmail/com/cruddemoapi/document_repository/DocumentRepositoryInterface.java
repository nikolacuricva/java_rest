package nikolacuricva.gmail.com.cruddemoapi.document_repository;

import nikolacuricva.gmail.com.cruddemoapi.document.EmployeeDoc;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentRepositoryInterface extends MongoRepository<EmployeeDoc, String> {
}
